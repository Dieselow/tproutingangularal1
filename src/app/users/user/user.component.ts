import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: { id: number, name: string };

  constructor(private userService: UserService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('userId');
    this.user = this.userService.getUser(+id);

    this.route.paramMap.subscribe((paramMap)=> {
      this.user = this.userService.getUser(Number(paramMap.get("userId")));
    })
  }

}
