import {Component, OnInit} from '@angular/core';

import {ServersService} from '../servers.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {
  server: { id: number, name: string, status: string };

  constructor(private serversService: ServersService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('serverId');
    this.server = this.serversService.getServer(+id);

    this.route.paramMap.subscribe((paramMap) => {
      this.server = this.serversService.getServer(Number(paramMap.get("serverId")));
    });
  }

  async navigateToEdit() {
   await  this.router.navigate(['edit'], {relativeTo: this.route});
  }
}
